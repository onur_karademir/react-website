import React from "react";
import { Navbar } from "./components";
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import Home from "./pages/HomePage/Home";
import ScrollToTop from "./components/ScrooltoTop";
import Contact from "./pages/Contacts/Contact";
import Singup from "./components/Singup/Singup";
// import { Button, Alert } from "reactstrap";

// import { v4 as uuidv4 } from 'uuid'; /// uuid import exemple //

//if you want use axios -->>> import_npm_list.txt inside axios you can learn from web page//

function App() {
  return (
    <BrowserRouter>
    <ScrollToTop />
      <Navbar />
      <Switch>
        <Route path="/" exact component={Home}/>
        <Route path="/contact" exact component={Contact}/>
        <Route path="/sing-up" exact component={Singup}/>
      </Switch>
      <Redirect to="/"></Redirect>
    </BrowserRouter>
  );
}

export default App;
