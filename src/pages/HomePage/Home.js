import React from "react";
import { ContactSection, InfoSection } from "../../components";
import { homeObjOne, homeObjTwo, homeObjThree,contactObjOne } from "./Data";
const Home = () => {
  return (
    <>
      <InfoSection {...homeObjOne} />
      <InfoSection {...homeObjTwo} />
      <ContactSection {...contactObjOne} />
      <InfoSection {...homeObjThree} />
    </>
  );
};

export default Home;
