import React from "react";
import { ContactSection, InfoSection } from "../../components";
import { homeObjThree, contactObjOne } from "./Data";

const Home = () => {
  return (
    <>
      <ContactSection {...contactObjOne} />
      <InfoSection {...homeObjThree} />
    </>
  );
};

export default Home;
