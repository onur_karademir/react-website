export const homeObjOne = {
  topLine: "Marketing Agency",
  headline: "What is Lorem Ipsum?",
  description:
    "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
  img:require('../../images/1.svg'),
  order:"order-md-1",
  orderText:"order-md-2",
  content: [{name:"onur"},{name:"aiden"},{name:"neo"}],
  buttonLabel: "Get Started"
};
export const homeObjTwo = {
    topLine: "Marketing Agency",
    headline: "What is Lorem Ipsum?",
    description:
      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
      order:'order-md-2',
      orderText:"order-md-1",
      img:require('../../images/1.svg'),
      buttonLabel: "Get Started"
  };

  export const homeObjThree = {
    topLine: "Marketing Agency",
    headline: "What is Lorem Ipsum?",
    description:
      "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
      order:"order-md-1",
      orderText:"order-md-2",
      img:require('../../images/3.svg'),
      buttonLabel: "Get Started"
  };
  export const homeObjData = {
  content: [{name:"onur"},{name:"aiden"},{name:"neo"}]
  };
  export const contactObjOne = {
    goldPrice: "$299.00",
    goldHeadline: "Gold",
    silverPrice: "$199.00",
    silverHeadline: "Silver",
    bronzePrice: "$99.00",
    bronzeHeadline: "Bronze",
  };
