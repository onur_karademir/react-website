export { default as Navbar } from "./Navbar/NavbarSection";

export { default as InfoSection } from "./InfoSection/InfoSection";

export { default as ContactSection } from "./ContactSection/ContactSection";

export { default as List } from "./InfoSection/List";
