import styled from "styled-components";


export const InfoSectionContainer = styled.div`
`
export const Wrapper = styled.div`
background-color:#05012b;
padding: 40px 10px;
`
export const TextContainer = styled.div`
@media screen and (max-width:960px) {
    text-align:center;
}
`
export const Text = styled.p`
color:#fff;
text-align:justify;
`
export const TextHead = styled.h2`
color:#fff;
`
export const ImageContainer = styled.div`
max-width:450px;
@media screen and (max-width:960px) {
    display:block;
    margin:0 auto;
}
`
export const Img = styled.img`
width:100%;
`
export const ButtonLabel = styled.button`
background-color:#1e0fa1;
color:#fff;
outline: none;
border: none;
cursor: pointer;
padding:10px 20px;
white-space: nowrap;
border-radius: 4px;
box-shadow: inset 0 0 0 0 #fff;
transition:ease-out .3s;
font-weight:600;
&:hover{
    box-shadow: inset 150px 0 0 0 #fff;
    color:#1e0fa1;
}
`