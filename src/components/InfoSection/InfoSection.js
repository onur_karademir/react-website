import React from "react";
// import { List } from "..";
import {InfoSectionContainer,Wrapper,TextContainer,Text,ImageContainer,Img,ButtonLabel,TextHead} from "./InfoSection.elements";
// import {homeObjData} from "../../pages/HomePage/Data";
const InfoSection = ({headline,description,img,order,orderText,buttonLabel}) => {
    return(
        <Wrapper>
        <InfoSectionContainer className="container p-5">
            <div className="row">
                <div className={`col-md-12 col-sm-12 col-lg-6 ${orderText}`}>
                        <TextContainer>
                            <TextHead>
                                {headline}
                            </TextHead>
                            <Text>
                                {description}
                            </Text>
                            <ButtonLabel>
                                {buttonLabel}
                            </ButtonLabel>
                    </TextContainer>
                </div>
                <div className={`col-md-12 col-sm-12 col-lg-6 ${order}`}>
                <ImageContainer>
                    <Img className="img-thumbnail img-fluid" src={img} alt="info-section"></Img>
                </ImageContainer>
                </div>
            </div>
        </InfoSectionContainer>
        </Wrapper>
      
    );
};


export default InfoSection;