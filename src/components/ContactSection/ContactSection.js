import React from "react";
import {ContactSectionContainer,CardContainer,Text,ImageContainer,Icon,Price,PriceButton} from "./ContactSection.elements";
import {IconContext } from "react-icons/lib"
const ContactSection = ({goldHeadline,goldPrice,silverHeadline,silverPrice,bronzeHeadline,bronzePrice}) => {
    return(
        <IconContext.Provider value={{color:'#fff'}}>
        <ContactSectionContainer>
            <div className="w-75">
            <CardContainer>
            <ImageContainer>
            </ImageContainer>
                    <Icon />
                    <Text>
                        {goldHeadline}
                    </Text>
                    <Price>
                        {goldPrice}
                    </Price>
                    <PriceButton>
                        {goldHeadline}
                    </PriceButton>
            </CardContainer>
            <CardContainer>
            <ImageContainer>
            </ImageContainer>
                    <Icon />
                    <Text>
                        {silverHeadline}
                    </Text>
                    <Price>
                        {silverPrice}
                    </Price>
                    <PriceButton>
                        {silverHeadline}
                    </PriceButton>
            </CardContainer>
            <CardContainer>
            <ImageContainer>
            </ImageContainer>
                    <Icon />
                    <Text>
                        {bronzeHeadline}
                    </Text>
                    <Price>
                        {bronzePrice}
                    </Price>
                    <PriceButton>
                        {bronzeHeadline}
                    </PriceButton>
            </CardContainer>
            </div>
        </ContactSectionContainer>
        </IconContext.Provider>
    );
};


export default ContactSection;