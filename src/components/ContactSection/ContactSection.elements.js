import styled from "styled-components";

import {GiCutDiamond} from "react-icons/gi";

export const ContactSectionContainer = styled.div`
display:flex;
flex-direction:row;
justify-content:space-around;
background-color:#1e0fa1;
text-align:center;
padding: 40px 10px;
`
export const CardContainer = styled.div`
background-color:#292124;
width:300px;
height: 350px;
display:inline-block;
margin:5px;
border-radius:10px;
position:relative;
padding:2rem;
transition:all .5s ease-in-out;
&:hover {
    transform:scale(1.05);
    cursor:pointer;
}
`
export const Text = styled.h3`
color:#fff;
display: block;
margin:1rem auto;
`
export const Price = styled.h1`
color:#fff;
display: block;
margin:1rem auto;
`
export const ImageContainer = styled.div`
overflow: hidden;
`
export const Img = styled.img`
width:75%;
transition:all .5s ease-in-out;
z-index:-1;
&:hover{
transform: scale(1.1);
}
`;

export const Icon = styled(GiCutDiamond)`
font-size:4rem;
`;

export const PriceButton = styled.button `
background-color:#1e0fa1;
color:#fff;
outline: none;
border: none;
cursor: pointer;
padding:10px 20px;
white-space: nowrap;
border-radius: 4px;
box-shadow: 8px 8px 5px 0px rgba(0,0,0,0.75);
margin:10px 0;
`