import styled from "styled-components";
import {Link} from "react-router-dom";
import { FaMagento } from "react-icons/fa";

export const NavContainer = styled.nav`
display:flex;
justify-content:space-around;
/* position:static;
top:0; */
width:100%;
height: 80px;
background-color:#292124;
`;
export const NavbarBrand = styled(Link)`
display:flex;
justify-self:flex-start;
align-items:center;
cursor:pointer;
text-decoration:none;
font-weight:700;
color:white;
font-size:2rem;
&:hover{
    text-decoration:none;
    color:#fff;
}
`;

export const NavIcon = styled(FaMagento)`
margin:0 .5rem;
`;

export const NavUl = styled.ul`
display:flex;
align-items:center;
/* align-self: flex-end; */
text-align:center;
list-style:none;
@media screen and (max-width:960px){
display:flex;
flex-direction:column;
width: 100%;
height: 90vh;
position:absolute;
top:80px;
background-color : #05012b;
transition : all .5s ease;
left:${({click}) => (click ? 0 : '-100%')};
}
`;

export const NavLi = styled.li`
border-bottom:2px solid transparent;
font-weight:700;
color:#fff;
margin:0 1rem;
&:hover {
    border-bottom:2px solid #ffffff;
    cursor: pointer;
}
@media screen and (max-width:960px){
width: 100%;
padding:2rem;
display:flex;
justify-content:center;
align-items:center;
&:hover {
    border-bottom:2px solid transparent;
}
}

`;
export const NavLink = styled(Link)`
display:flex;
align-items:center;
text-decoration:none;
color:#fff;
&:hover{
    color:#fff;
    text-decoration:none;
}
`;
export const NavButtonElement =styled.li `
background-color:#1e0fa1;
color:#fff;
outline: none;
border: none;
cursor: pointer;
padding:10px 20px;
white-space: nowrap;
border-radius: 4px;
box-shadow: 8px 8px 5px 0px rgba(0,0,0,0.75);
@media screen and (max-width:960px){
display:flex;
justify-content:center;
align-items:center;
width: 50% !important;
}
`;

export const ButtonLink =styled(Link) `
display:flex;
justify-content:center;
align-items:center;
color:#fff;
text-decoration:none;
width: 100%;
height:100%;
&:hover {
color:#fff;
text-decoration:none;
}
`;

export const MobileIcon = styled.li`
display:none;
font-weight:700;
font-size:2rem;
@media screen and (max-width : 960px){
    display: block;
}
`;


