import React, { useState, useEffect } from "react";
import {FaBars,FaTimes} from "react-icons/fa";
import { IconContext } from "react-icons/lib";
import {NavContainer, NavbarBrand, MobileIcon, NavIcon,NavUl,NavLi,NavLink, NavButtonElement, ButtonLink} from './NavbarSection.elements';
const Navbar = () => {
    const [click,setClick] = useState(false);
    const clickHendler = () => setClick(!click);
    const [button, setButton] = useState(true);
const showButton = () => {
    if (window.innerWidth <= 960) {
        setButton(false);
    } else {
        setButton(true);
    }
};
useEffect(()=> {
    showButton();
},[]);

window.addEventListener('resize',showButton);
    return(
        <>
        <IconContext.Provider value = {{color:"#fff"}}>
            <NavContainer className="navbar fixed-top">
                <NavbarBrand to="/">
                    <NavIcon />
                        BRAND
                </NavbarBrand>
                <MobileIcon onClick={clickHendler}>
                    {click ? <FaTimes /> : <FaBars/>}
                </MobileIcon>
                <NavUl onClick={clickHendler} click={click}>
                    <NavLi>
                        <NavLink to="/">
                            HOME
                        </NavLink>
                    </NavLi>
                    <NavLi>
                        <NavLink to="/contact">
                            PRICES
                        </NavLink>
                    </NavLi>
                    <NavLi>
                        <NavLink to="/">
                             ABOUT
                        </NavLink>
                    </NavLi>
                    <NavButtonElement>
                    {
                    button ? (<ButtonLink to='/sing-up'>SING UP</ButtonLink>) : (<ButtonLink to='/sing-up'>SING UP</ButtonLink>)
                    }
                </NavButtonElement>
                </NavUl>
            </NavContainer>
            </IconContext.Provider>
        </>
    );
};


export default Navbar;